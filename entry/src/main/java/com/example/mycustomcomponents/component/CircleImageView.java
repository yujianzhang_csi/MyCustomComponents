package com.example.mycustomcomponents.component;

import com.example.mycustomcomponents.component.ImageEx.ImageEx;
import com.example.mycustomcomponents.util.Log;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

public class CircleImageView extends ImageEx implements Component.DrawTask {
    public static final String TAG = CircleImageView.class.getSimpleName();

    private static final int DEFAULT_BORDER_WIDTH = 6;
    private static final Color DEFAULT_BORDER_COLOR = Color.CYAN;

    private final int borderWidth = DEFAULT_BORDER_WIDTH;
    private final Color borderColor = DEFAULT_BORDER_COLOR;

    private final Paint paint = new Paint();
    
    public CircleImageView(Context context) {
        super(context);
    }

    public CircleImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public CircleImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    {
        addDrawTask(this);
    }

    private PixelMap createPixelMap(int width, int height) {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(width, height);
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        return PixelMap.create(options);
    }

    private PixelMap createScaledPixelMap(PixelMap srcImage, int dstWidth, int dstHeight) {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(dstWidth, dstHeight);
        return PixelMap.create(srcImage, options);
    }

    /**
     * 把矩形图片裁剪成正方形图片
     *
     * @param image
     * @return
     */
    private PixelMap squareImage(PixelMap image) {
        int bitmapWidth = image.getImageInfo().size.width;
        int bitmapHeight = image.getImageInfo().size.height;
        int squareSide = 0;
        int x = 0;
        int y = 0;
        PixelMap squareImage = null;

        if (bitmapHeight > bitmapWidth) {
            squareSide = bitmapWidth;
            y = (bitmapHeight - bitmapWidth) / 2;
        } else if (bitmapHeight < bitmapWidth) {
            squareSide = bitmapHeight;
            x = (bitmapWidth - bitmapHeight) / 2;
        } else {
            squareImage = image;
        }

        if (squareImage == null) {
            squareImage = PixelMap.create(image, new Rect(x, y, squareSide, squareSide), null);
        }

        return squareImage;
    }

    /**
     * 把矩形图片转换成圆形图片
     *
     * @param image 原始图片最好为正方形，否则会因为拉伸而失真。
     * @return
     */
    private PixelMap circleImage(PixelMap image) {
        int width = image.getImageInfo().size.width;
        int height = image.getImageInfo().size.height;

        PixelMap circleImage = createPixelMap(width, height);
        Canvas canvas = new Canvas(new Texture(circleImage));

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);

        canvas.drawCircle(width / 2f, height / 2f, width / 2f, paint);
        paint.setBlendMode(BlendMode.SRC_IN);

        RectFloat rect = new RectFloat(0, 0, width, height);
        canvas.drawPixelMapHolderRect(new PixelMapHolder(image), rect, rect, paint);

        return circleImage;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        Log.d(TAG, "onDraw");
        PixelMap originalImage = ((PixelMapElement) getImageElement()).getPixelMap();
        Log.d(TAG, "originalImage.info=" + originalImage.getImageInfo());

        if (originalImage == null || getWidth() == 0 || getHeight() == 0) {
            return;
        }

        // 处理 padding
        int defaultWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        int defaultHeight = getHeight() - getPaddingTop() - getPaddingBottom();
        int scaleSize = Math.min(defaultWidth, defaultHeight);
        float top = getPaddingTop() + (defaultHeight - scaleSize) / 2f;
        float left = getPaddingLeft() + (defaultWidth - scaleSize) / 2f;

        // borderExtra 用来处理 border 与图片的边缘
        int borderExtra = 0;
        if (borderWidth > 0) {
            borderExtra = 1;
        }

        // 创建方形图片
        PixelMap squareImage = squareImage(originalImage);

        // 创建去掉 border 的方形图片
        PixelMap cutBorderSquare = createScaledPixelMap(
                squareImage, scaleSize - borderWidth * 2 + borderExtra, scaleSize - borderWidth * 2 + borderExtra);

        // 创建圆形图片
        PixelMap circleImage = circleImage(cutBorderSquare);

        // 将圆形图片绘制到 canvas 上
        canvas.drawPixelMapHolder(new PixelMapHolder(circleImage),
                borderWidth - borderExtra / 2f + left,
                borderWidth - borderExtra / 2f + top,
                paint);

        // 绘制 border
        drawCircleBorder(canvas,
                scaleSize / 2f + left,
                scaleSize / 2f + top,
                scaleSize / 2f);
    }

    /**
     * 画边框
     * @param canvas
     * @param x
     * @param y
     * @param radius
     */
    private void drawCircleBorder(Canvas canvas, float x, float y, float radius) {
        if (borderWidth <= 0)
            return;

        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(borderColor);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setStrokeWidth(borderWidth);
        canvas.drawCircle(x, y, radius - borderWidth / 2f, paint);
    }
}
