package com.example.mycustomcomponents.component.ImageEx;

/**
 * Options for scaling the bounds of an image to the bounds of this view.
 */
public enum ScaleType {
    matrix(0),

    /**
     * 对X和Y方向独立缩放，直到图片铺满ImageView。这种方式可能会改变图片原本的宽高比，导致图片拉伸变形。
     */
    fitXY(1),

    /**
     * 保持图片的宽高比，对图片进行X和Y方向缩放，直到一个方向铺满ImageView。
     * 缩放后的图片与ImageView左上角对齐进行显示。
     */
    fitStart(2),

    /**
     * 保持图片的宽高比，对图片进行X和Y方向缩放，直到一个方向铺满ImageView。
     * 缩放后的图片居中显示在ImageView中。
     */
    fitCenter(3),

    /**
     * 保持图片的宽高比，对图片进行X和Y方向缩放，直到一个方向铺满ImageView。
     * 缩放后的图片与ImageView右下角对齐进行显示。
     */
    fitEnd(4),

    // 图片居中显示在ImageView中，不对图片进行缩放。
    center(5),

    /**
     * 保持图片的宽高比，等比例对图片进行X和Y方向缩放，直到每个方向都大于等于ImageView对应的尺寸。
     * 缩放后的图片居中显示在ImageView中，超出部分做裁剪处理
     */
    centerCrop(6),

    /**
     * 如果图片宽度<=ImageView宽度&&图片高度<=ImageView高度，不执行缩放，居中显示在ImageView中。
     * 其余情况按ScaleType.FIT_CENTER处理
     */
    centerInside(7);

    ScaleType(int intValue) {
        this.intValue = intValue;
    }
    final int intValue;
}
