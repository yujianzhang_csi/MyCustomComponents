package com.example.mycustomcomponents.component.ImageEx;

import com.example.mycustomcomponents.util.Log;
import com.example.mycustomcomponents.util.Utils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Matrix;
import ohos.app.Context;

import java.util.Arrays;

/**
 * Owner draw Image support matrix
 */
public class ImageEx extends Component implements Component.DrawTask {
    public static final String TAG = ImageEx.class.getSimpleName();

    private Element element;
    private ScaleType scaleType = ScaleType.fitCenter;
    private final Matrix matrix = new Matrix();

    public ImageEx(Context context) {
        super(context);
    }

    public ImageEx(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public ImageEx(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    public ImageEx(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        readAttrSet(attrSet);
        dealScaleType();
        addDrawTask(this);
    }

    private void dealScaleType() {
        if (getElementWidth() <= 0 || getElementHeight() <= 0) {
            element.setBounds(0, 0, getDisplayWidth(), getDisplayHeight());
            return;
        }

        switch (scaleType) {
            default:
            case matrix:
                break;
            case fitCenter:
                fitCenter();
                break;
            case center:
                center();
                break;
            case fitXY:
                fitXY();
                break;
            case fitStart:
                fitStart();
                break;
            case fitEnd:
                fitEnd();
                break;
            case centerCrop:
                centerCrop();
                break;
            case centerInside:
                centerInside();
                break;
        }
    }

    private void readAttrSet(AttrSet attrSet) {
        element = attrSet.getAttr("src").get().getElement();
        Log.d(TAG, "src width=" + element.getWidth());
        Log.d(TAG, "src bounds=" + Arrays.toString(element.getBounds().getRectSize()));

        if (attrSet.getAttr("scaleType").isPresent()) {
            String scaleTypeString = attrSet.getAttr("scaleType").get().getStringValue();
            scaleType = Utils.getEnumFromString(ScaleType.class, scaleTypeString, ScaleType.center);
        }
    }

    private int getDisplayWidth() {
        return getWidth() - getPaddingLeft() - getPaddingRight();
    }

    private int getDisplayHeight() {
        return getHeight() - getPaddingLeft() - getPaddingRight();
    }

    /**
     * 对X和Y方向独立缩放，直到图片铺满ImageView。这种方式可能会改变图片原本的宽高比，导致图片拉伸变形。
     */
    private void fitXY(){
        float wPercent = (float)getDisplayWidth() / (float)getElementWidth();
        float hPercent = (float)getDisplayHeight() / (float)getElementHeight();
        Log.d(TAG, String.format("fitXY, scale wPercent=%f, hPercent=%f", wPercent, hPercent));
        matrix.setScale(wPercent, hPercent);
    }

    /**
     * 保持图片的宽高比，对图片进行X和Y方向缩放，直到一个方向铺满ImageView。
     * 缩放后的图片与ImageView左上角对齐进行显示。
     */
    private void fitStart() {
        float wPercent = (float)getDisplayWidth() / (float)getElementWidth();
        float hPercent = (float)getDisplayHeight() / (float)getElementHeight();
        float minPercent = Math.min(wPercent, hPercent);
        Log.d(TAG, String.format("fitStart, scale percent=%f", minPercent));
        matrix.setScale(minPercent, minPercent);
    }

    /**
     * 保持图片的宽高比，对图片进行X和Y方向缩放，直到一个方向铺满ImageView。
     * 缩放后的图片与ImageView右下角对齐进行显示。
     */
    private void fitEnd() {
        float wPercent = (float)getDisplayWidth() / (float)getElementWidth();
        float hPercent = (float)getDisplayHeight() / (float)getElementHeight();
        float minPercent = Math.min(wPercent, hPercent);

        float targetWidth = minPercent * getElementWidth();
        float targetHeight = minPercent * getElementHeight();

        float wTranslate = getDisplayWidth() - targetWidth;
        float hTranslate = getDisplayHeight() - targetHeight;

        Log.d(TAG, String.format("fitEnd, scale percent=%f, wTranslate=%f, hTranslate=%f", minPercent, wTranslate, hTranslate));
        matrix.setScale(minPercent, minPercent);
        matrix.postTranslate(wTranslate, hTranslate);
    }

    /**
     * 保持图片的宽高比，对图片进行X和Y方向缩放，直到一个方向铺满ImageView。
     * 缩放后的图片居中显示在ImageView中。
     */
    private void fitCenter() {
        float wPercent = (float)getDisplayWidth() / (float)getElementWidth();
        float hPercent = (float)getDisplayHeight() / (float)getElementHeight();
        float minPercent = Math.min(wPercent, hPercent);

        float targetWidth = minPercent * getElementWidth();
        float targetHeight = minPercent * getElementHeight();

        float wTranslate = (getDisplayWidth() - targetWidth) * 0.5f;
        float hTranslate = (getDisplayHeight() - targetHeight) * 0.5f;

        Log.d(TAG, String.format("fitCenter, scale percent=%f, wTranslate=%f, hTranslate=%f", minPercent, wTranslate, hTranslate));
        matrix.setScale(minPercent, minPercent);
        matrix.postTranslate(wTranslate, hTranslate);
    }

    /**
     * 图片居中显示在ImageView中，不对图片进行缩放
     */
    private void center() {
        float wTranslate = (getDisplayWidth() - getElementWidth()) * 0.5f;
        float hTranslate = (getDisplayHeight() - getElementHeight()) * 0.5f;

        Log.d(TAG, String.format("center, wTranslate=%f, hTranslate=%f", wTranslate, hTranslate));
        matrix.postTranslate(wTranslate, hTranslate);
    }

    /**
     * 如果图片宽度<=ImageView宽度&&图片高度<=ImageView高度，不执行缩放，居中显示在ImageView中。
     * 其余情况按ScaleType.FIT_CENTER处理。
     */
    private void centerInside() {
        if (getElementWidth() <= getDisplayWidth() && getElementHeight() <= getElementHeight()){
            float wTranslate = (getDisplayWidth() - getElementWidth()) * 0.5f;
            float hTranslate = (getDisplayHeight() - getElementHeight()) * 0.5f;

            Log.d(TAG, String.format("centerInside, wTranslate=%f, hTranslate=%f", wTranslate, hTranslate));
            matrix.setTranslate(wTranslate, hTranslate);
        } else{
            fitCenter();
        }
    }

    /**
     * 保持图片的宽高比，等比例对图片进行X和Y方向缩放，直到每个方向都大于等于ImageView对应的尺寸。
     * 缩放后的图片居中显示在ImageView中，超出部分做裁剪处理。
     */
    private void centerCrop() {
        float scale;
        float dx;
        float dy;

        if (getElementWidth() * getDisplayHeight() > getDisplayWidth() * getElementHeight()) {
            scale = (float)getDisplayHeight() / (float)getElementHeight();
            dx = (getDisplayWidth() - getElementWidth() * scale) * 0.5f;
            dy = 0f;
        } else {
            scale = (float)getDisplayWidth() / (float)getElementWidth();
            dx = 0f;
            dy = (getDisplayHeight() - getElementHeight() * scale) * 0.5f;
        }

        Log.d(TAG, String.format("centerCrop, scale=%f, dx=%f, dy=%f", scale, dx, dy));
        matrix.setScale(scale, scale);
        matrix.postTranslate(dx + 0.5f, dy + 0.5f);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        Log.d(TAG, "onDraw");
        canvas.concat(matrix);
        element.drawToCanvas(canvas);
    }

    private int getElementWidth() {
        return element.getWidth();
    }

    private int getElementHeight() {
        return element.getHeight();
    }

    public Element getImageElement() {
        return element;
    }

    public void setImageMatrix(Matrix matrix) {
        if (matrix == null) {
            return;
        }

        matrix.setMatrix(matrix);
        dealScaleType();
        invalidate();
    }
}
