package com.example.mycustomcomponents.util;

public class Utils {
    /**
     * 一个对于所有Enum类通用的方法，因为他们不能有另一个基类
     * @param <T> Enum type
     * @param c enum type. All enums must be all caps.
     * @param string case insensitive
     * @return corresponding enum, or null
     */
    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string, T defaultValue) {
        if( c != null && string != null ) {
            try {
                return Enum.valueOf(c, string);
            } catch(IllegalArgumentException ex) {
                Log.e("getEnumFromString", String.format("%s convert to enum failed!", string));
            }
        }
        return defaultValue;
    }

}
